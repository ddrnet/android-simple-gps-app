package com.example.gpsapplication;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.nabinbhandari.android.permissions.PermissionHandler;
import com.nabinbhandari.android.permissions.Permissions;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity
{

    private ArrayList<String> permissionsToRequest;
    private ArrayList<String> permissionsRejected = new ArrayList();
    private final String[] permissions = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.INTERNET};
    private final static int ALL_PERMISSIONS_RESULT = 101;

    Location loc;
    TextView lat, lon, serverResponce;
    double longitude, latitude;
    String responseText = "";
    private Handler mHandler = new Handler(Looper.getMainLooper());

    @SuppressLint("ResourceType")
    private final String URL ="https://api.bigdatacloud.net/data/reverse-geocode-client?latitude=:lat&longitude=:lon&localityLanguage=ru";
    OkHttpClient client = new OkHttpClient();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lon = findViewById(R.id.Lon_tv);
        lat = findViewById(R.id.Lat_tv);
        serverResponce = findViewById(R.id.serverResponce);
        Button exit = findViewById(R.id.btn_exit);
        exit.setOnClickListener(new View.OnClickListener()
        {
                                    @Override
                                    public void onClick(View view)
                                    {
                                        android.os.Process.killProcess(android.os.Process.myPid());
                                    }
                                });
        callPermissions();
    }

    public void callPermissions()
    {
        String rationale = "Необходим доступ к информации о геолокации...";
        Permissions.Options options = new Permissions.Options()
                .setRationaleDialogTitle("Info")
                .setSettingsDialogTitle("Warning");
        Permissions.check(this, permissions, rationale, options, new PermissionHandler()
        {
            @Override
            public void onGranted()
            {
                requestLocationUpdate();
                Button btn = findViewById(R.id.btn);
                btn.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                        if(latitude != 0 && longitude != 0)
                        {

                            String requestStr = URL
                                    .replace(":lat", Double.toString(latitude))
                                    .replace(":lon", Double.toString(longitude));
                            GetGPSServerInfo(requestStr, new Callback()
                            {
                                @Override
                                public void onFailure(@NotNull Call call, @NotNull IOException e)
                                {
                                    Log.d("ERROR_GPSServerResponce", e.getMessage());
                                }
                                @Override
                                public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException
                                {
                                    responseText  = response.body().string();
                                    mHandler.post(new Runnable()
                                    {
                                        @Override
                                        public void run()
                                        {
                                            serverResponce.setText(responseText);
                                        }
                                    });
                                    Log.d("GPSServerResponce", responseText);
                                }
                            });
                        }
                        Toast.makeText(getApplicationContext(), "Получаем подробную информацию", Toast.LENGTH_LONG).show();
                    }
                });
            }
            @Override
            public void onDenied(Context context, ArrayList<String> deniedPermissions)
            {
                callPermissions();
            }
        });
    }

    void GetGPSServerInfo(String url, Callback callback)
    {
        Request request = new Request.Builder()
                .url(url)
                .build();
        Call call = client.newCall(request);
        call.enqueue(callback);
    }

    public void requestLocationUpdate()
    {
        FusedLocationProviderClient fusedLocationClient = new FusedLocationProviderClient(this);
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setFastestInterval(2000).setInterval(4000);
        fusedLocationClient.requestLocationUpdates(locationRequest, new LocationCallback()
        {

            @Override
            public void onLocationResult(LocationResult locationResult)
            {
                super.onLocationResult(locationResult);
                loc = locationResult.getLastLocation();
                latitude = loc.getLatitude();
                longitude = loc.getLongitude();
                lat.setText("Широта: " + latitude);
                lon.setText("Долгота: " + longitude);
                Log.d("gpsCoords", latitude + " " + longitude);
            }
        }, getMainLooper());
    }

    private ArrayList findUnAskedPermissions(List<String> wanted)
    {
        ArrayList result = new ArrayList();
        for (Object perm : wanted)
        {
            if (!hasPermission(perm.toString()))
            {
                result.add(perm);
            }
        }

        return result;
    }

    private boolean hasPermission(String permission)
    {
        if (canMakeSmores())
        {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    private boolean canMakeSmores()
    {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }


    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NotNull String[] permissions, @NotNull int[] grantResults)
    {

        if (requestCode == ALL_PERMISSIONS_RESULT)
        {
            for (String perms : permissionsToRequest)
            {
                if (!hasPermission(perms))
                {
                    permissionsRejected.add(perms);
                }
            }

            if (permissionsRejected.size() > 0)
            {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                {
                    if (shouldShowRequestPermissionRationale(permissionsRejected.get(0)))
                    {
                        showMessageOKCancel(
                                new DialogInterface.OnClickListener()
                                {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which)
                                    {
                                        requestPermissions(permissionsRejected.toArray(new String[0]), ALL_PERMISSIONS_RESULT);
                                    }
                                });
                    }
                }
            }
        }

    }

    private void showMessageOKCancel(DialogInterface.OnClickListener okListener)
    {
        new AlertDialog.Builder(MainActivity.this)
                .setMessage("Эти разрешения являются обязательными для приложения. Пожалуйста, разрешите доступ.")
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }
}
