# Просмотр GPS информации

Просто приложение для Android (5.1.1+) \
**APK: [Скачать](App.apk)**

<img src="Screenshot_2020-03-23-00-28-38.png" height="500" />
<img src="Screenshot_2020-03-23-00-28-26.png" height="500" />

Функции:
* Отображение GPS локации
* Обращение к сервису bigdatacloud.net за подробностями о текущих координатах

Использованные библиотеки: 
 * Для работы с геодатой `play-services-location:17.0.0`
 * Для отправки запросов `okhttp:4.4.0`
 * Для проверки разрешений пользователя `'com.nabinbhandari.android:permissions:3.8'`